# TCP/IP

![img](./osi_tcp_ip.png)


## Vad är `localhost`?

Localhost är ett hostnamn som alltid pekar mot s.k. `loopback address`, `127.0.0.1`, som är en lokal nätverksinterface.

Egentligen hela `127.0.0.0/8` nätverket mappar till `loopback`, så alla adresser i rangen `127.0.0.1 - 127.255.255.254` kan användas lokalt.


## DNS

DNS mappar hostnamn till ip-adresser.


### Byta DNS server och skapa manuella dns mappningar

I Linux DNS servern som används av systemet styrs av filen `/etc/resolv.conf`.

`/etc/hosts` kan användas för att skapa lokala mappningar.


### Felsökning av DNS

När man felsöker nätverksproblem, börja alltid med att säkerhetställa att DNS fungerar. Exempel:

`ping google.com` om detta failar, testa

`ping 8.8.8.8` (`8.8.8.8` äddressen ägs av google, den går alltid att pinga) om detta inte failar men föregående failar, så funkar inte DNS. Dvs man kan pinga google via ip address men inte hostnamn.

`8.8.8.8`, `4.4.4.4` (google) och `1.1.1.1` (cloudflare) är exempel på populära publika DNS servrar man kan använda.


### 

![img](./dns_haiku.webp)


# Python

Python är en ****interpreted**** language och inte ****compiled****.

I en ****compiled**** language körs källkoden igenom en compiler som spottar ut en binär executable (maskinkod) (i.e. en `.exe` fil i Windows).

I en ****interpreted**** language körs källkoden igenom en ****interpreter**** vid varje körning.

Som tumregel, compiled språk är ofta snabbare (dock kompilering kan ta tid) och interpreted språk är långsammare. I vissa fal är dessa hastighetsskillnader relevanta, men oftast är körtid på maskin ganska billig för företag, medans utvecklartid är dyrare. Interpreted språk är ofta snabbare/smidigare att utveckla i.


## Funtkioner

En funktion är en snippet av kod man definierar och namnger. Man kan sedan kalla på det, och gör så att det blir mindre repetition i vår kod. Man ska försöka undvika repetition i koden så mycket det går (men använd sund förnuft här).

Live exempel på funktioner, och argument.


## Object Oriented

Python är en s.k. objected oriented language.

Ett objekt är en instans av en `class`. En `class` är själva "blueprint" för objekt. Det är en datastruktur som kan innehålla variabler (`attributes`) och funktioner (`methods`). En `class` är också ett sätt att undvika repetition i koden.

Ett objekt sägs ha en typ (`type`) som indikerar vilken klass den kommer ifrån. En klass kan ärva (`inherit`) attributer och metoder från en eller flera andra klasser.

Live exempel på `class`, `inheritance`, `class.mro`


### Vad i Python är ett objekt?

****ALLT I PYTHON ÄR ETT OBJEKT****


### &#x2026;?

Vad menas med "allt i python är ett objekt"?


### 

-   är en variabel ett objekt?


### 

-   är en funktion ett objekt?


### 

-   är en modul man importerar ett objekt?


### 

-   är en class ett objekt?


## Import

`import` använder vi för att ladda i externa moduler och packeter i vårt kod.

Exempel på import:

`import math`

`from math import factorial`

`from math import factorial as fact`

Fråga: tror ni det finns en skillnad på hur mycket av källkoden i `math` modulen faktiskt importeras/körs mellan de 3 olika format på `import`?


## The almighty standard library

Python kommer med en stor stadard library. Som namnet tyder på är det en del av python standard release, så vi kan alltid anta att de moduler och paket kommer att finnas tillgängliga när python är installerat på systemet.

Trots den gamla skämtet *the standard library is where good libraries go and die* är standardlibrary mycket bra och användbar.

<https://docs.python.org/3/library/index.html>

Visa `functools`, `itertools`, `collections`.


## Concurrency

Concurrency eller parallelism (obs! de är inte exakt samma sak men skillnaden är för avancerad för oss just nu, så jag kommer använda de för att mena samma sak) är då ett program gör flera saker samtidigt (eller verkar göra det).

Två klassiska sätt att implementera parallelism eller concurrency är att använda `processes` eller `threads`.

Processer spawnar (skapar) ett helt nytt process (i.e. instans av `python` interpreter som körs av OS:et), medans trådar (`threads`) är ett sätt som OS:et ger oss att göra fler saker samtidigt från samma process.

Båda är någorlunda problematiska i Python.


### Processes

Att hantera flera processer från samma kod är ofta någorlunda osmidigt. Man måste då hålla koll på vilken som är grundprocessen och vilka är forks av det. Det finns utilities kring detta.

Live exempel på `concurrent.futures.ProcessPoolExecutor`.


### Threads

Threads i Python är också lite osmidiga i Python. Liknande utilities finns för att kringgå det.

Dessutom "lider" de flesta Python implementationer (inklusive CPython som vi använder) av något som kallas för `GIL` (Global Interpreter Lock) som gör så att bara 1 tråd i taget kan köra python kod.

Live exempel på `concurrent.futures.ThreadPoolExecutor`.


### There's gotta be a better way!

And there is.

There are third party libraries for handling concurrency/parallelism (i.e. Twisted).

There is also a new(`ish) syntax and standardlibrary module to the rescure: ~async/await` and `asyncio`.

OBS! `async/await` and `asyncio` are "contagious".

Live demo.


## When to use concurrency/parallelism?

There are (roughly speaking) 2 types of tasks that take time in a program:

-   computations (calculations carried out by the CPU)
-   I/O (input/output)

Concurrency/parallelism is only useful when dealing with I/O. Parallelising a heavy computation won't make it faster, while there usually is a deal of wait time when dealing with I/O.
